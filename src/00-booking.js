'use strict';

const webdriverio = require('webdriverio');
const IS_TEST = process.env.IS_TEST;
const SPORT = process.env.ACTIVITY;
const DATE = process.env.BOOKING_DATE;
const TIMESLOT = process.env.BOOKING_TIMESLOT;
const FIRST_NAME = process.env.FIRST_NAME;
const LAST_NAME = process.env.LAST_NAME;
const EMAIL = process.env.EMAIL;
const TEL_NUM = process.env.TEL_NUM;
const ID_TYPE = process.env.ID_TYPE;
const IC_NUMBER = process.env.ID_NUMBER;

var client = webdriverio.remote({
    host: 'localhost',
    port: '4444',
    desiredCapabilities: { browserName: 'phantomjs' }
});

var getTimeStamp = function(){
    return new Date().toTimeString();
};

var getTimeslotSelector = function(timeslot){
    return `//div[@class="bottom-buffer-line-dash"]
            //*[@class="col-xs-5 col-md-2"][contains(., "'`+timeslot+`'")]
            /following-sibling::div[2]/button[contains(@class, "btn-info")]`;
}

function initBrowserWithUrl(url){
    return client
                .init().then(function(){
                    client.emit('log', '['+getTimeStamp()+']: INITIALIZED BROWSER with url: '+url);
                })
                .url(url).then(function(){
                    client.emit('log', '['+getTimeStamp()+']: LOADED PAGE');
                })
}

function removeFirstVisitModal(){
    return client
                .click('#firstVisitModal button[data-dismiss="modal"]')
                .waitForVisible('#firstVisitModal .modal-footer button[data-dismiss="modal"]', 5000, true)
                .emit('log', '['+getTimeStamp()+']: Removed first time visit modal')
}

function selectActivity(){
    return client
                .selectByVisibleText('#community select', SPORT)
                .emit('log', '['+getTimeStamp()+']: Selected '+SPORT)
}

function selectDate(){
    return client
                .click('#kendoCalendar a[title="'+DATE+'"]')
                .emit('log', '['+getTimeStamp()+']: Selected '+DATE+'')

}

function addTimeslotToBasket(){
    return client
                .isVisible(getTimeslotSelector(TIMESLOT)).then(function(isVisible){
                    if(isVisible){

                        client
                            .click(getTimeslotSelector(TIMESLOT))
                            .emit('log', '['+getTimeStamp()+']: Booked timeslot: '+DATE+', '+TIMESLOT);

                    } else {
                        console.log('Booking for '+DATE+', '+TIMESLOT+' is not available');
                    }
                })

                .waitForVisible('#basketAddModal', 5000).then(function(isVisible){
                    if(isVisible){
                        client.click('#basketAddModal .modal-footer button.btn-primary')
                    }
                })

                .waitUntil(function() {
                    return client.url().then(function(url) {
                        return url.value.indexOf('action=basket') > -1;
                    });
                }, 5000)

                .waitForVisible('.modal-backdrop', 5000, true)
}

function fillDetails(){
    return client
                .setValue('input[data-bind^="value: first_name"]', FIRST_NAME)
                .setValue('input[data-bind^="value: surname"]', LAST_NAME)
                .setValue('input[data-bind^="value: customer_email"]', EMAIL)
                .setValue('input[data-bind^="value: telephone"]', TEL_NUM)
                .selectByValue('select[data-bind^="value: participate"]', ID_TYPE)
                .setValue('input[data-bind^="value: ic_number"]', IC_NUMBER)
                .waitForVisible('.text-success', 10000)
                .click('input[data-bind^="checked: agreedToTerms"]')
}

function submitOrCancelBooking(){
    if(IS_TEST === 'true'){
        return _removeBooking();
    } else {
        return _confirmBooking();
    }
}

function _removeBooking(){
    return client
                .click('button[data-bind="click: cancel"]')
                .pause(500)
                .emit('log', 'BOOKING REMOVED FOR TESTING');
}

function _confirmBooking(){
    return client
                .waitForEnabled('input#confirmBasket[type="submit"]', 5000)
                .click('input#confirmBasket[type="submit"]')
                .waitUntil(function(){
                    return client.url().then(function(url){
                        return url.value.indexOf('confirm.php') > -1;
                    });
                }, 10000)
                .emit('log', '['+getTimeStamp()+']: Booking for '+DATE+', '+TIMESLOT+' has been successful. Email has been sent to '+EMAIL+'!')
}

client.on('log', console.log);

var url = 'http://bookings.sportshub.com.sg/default.php?action=community';
initBrowserWithUrl(url)
    .call(removeFirstVisitModal)
    .call(selectActivity)
    .call(selectDate)
    .call(addTimeslotToBasket)
    .call(fillDetails)
    .call(submitOrCancelBooking)
    .end();
