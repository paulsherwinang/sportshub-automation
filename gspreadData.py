import json
import gspread
from oauth2client.client import SignedJwtAssertionCredentials

def authenticate():
	json_key = json.load(open('key.json'));
	scope = ['https://spreadsheets.google.com/feeds'];

	credentials = SignedJwtAssertionCredentials(json_key['client_email'], json_key['private_key'].encode(), scope);
	gc = gspread.authorize(credentials);

	return gc;

def get_data_from_sheet():
	gc = authenticate();

	sh = gc.open_by_key("1reyuFOcBc1BC9hSr2YuVo9Tbif6LvTlqXqi4_i5WXv0");
	ws = sh.worksheet('SportsHub')
	val = ws.get_all_values();
	val.pop(0);
	
	return val;