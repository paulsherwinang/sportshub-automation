var selenium = require('selenium-standalone');
var http = require('http');
var exec = require('child_process').exec;

function runSelenium(){
    selenium.install({
        version: '2.45.0',
        baseURL: 'https://selenium-release.storage.googleapis.com',
        drivers: {
            chrome: {
                version: '2.15',
                arch: process.arch,
                baseURL: 'https://chromedriver.storage.googleapis.com'
            }
        },
        logger: function(message) {
            console.log(message)
        }
    }, function(){
        selenium.start({
            spawnOptions: {
                stdio: 'inherit'
            }
        }, function(err, child) {
            console.log('Selenium started')
        });
    });
}


var url = 'http://localhost:4444/selenium-server/driver/?cmd=getLogMessages';

http.get(url, (res) => {
    if(res.statusCode == 200){
        exec('pkill -f selenium-standalone');
        runSelenium();
    }
})
.on('error', (e) => {
    runSelenium();
});