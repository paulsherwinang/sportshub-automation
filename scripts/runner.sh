#/!bin/sh

echo "======================================="
echo "SCHEDULE AUTOMATOR EXECUTED AT: $(date)"
echo "======================================="
export PATH=/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin
cd ~/Documents/sportshub-automation/

sleep 40
npm start
npm run stop_selenium
