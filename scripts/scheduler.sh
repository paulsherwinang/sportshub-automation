#/!bin/sh

echo "======================================="
echo "SCHEDULER EXECUTED AT: $(date)"
echo "======================================="
export PATH=/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin
cd ~/Documents/sportshub-automation/

npm run schedule_to_file
