#/!bin/sh

echo "======================================="
echo "TESTING SCHEDULE AUTOMATOR EXECUTED AT: $(date)"
echo "======================================="
export PATH=/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin
cd ~/Documents/sportshub-automation/

npm run schedule_to_file
IS_TEST=true npm start
# npm run stop_selenium
