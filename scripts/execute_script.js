'use strict';

const spawn = require('child_process').spawn;
const fs = require('fs');
const util = require('util')
const path = require('path');
const _ = require('lodash');
const dataJsonPath = path.resolve(__dirname, '../data.json');
const bookingScriptPath = path.resolve(__dirname, '../src/00-booking.js');
const logFilePath = path.resolve(process.env.HOME, 'Dropbox/logs/sportshub-automation.txt');
const dataSet = JSON.parse(fs.readFileSync(dataJsonPath, 'utf8'));

function constructEnvVariables(data){
    return Object.assign(process.env, {
        IS_TEST: process.env.IS_TEST || false,
        ACTIVITY: data[4],
        BOOKING_DATE: data[11]+', '+data[0]+' '+data[1]+', '+data[2],
        BOOKING_TIMESLOT: data[3],
        FIRST_NAME: data[5],
        LAST_NAME: data[6],
        EMAIL: data[7],
        TEL_NUM: data[8],
        ID_TYPE: data[9],
        ID_NUMBER: data[10]
    });
}

function spawnProcess(data){
    var args = [bookingScriptPath];
    var env = constructEnvVariables(data);
    var p = spawn('/usr/local/bin/node', args, { env: env });

    p.stdout.on('data', function(data) {
        var log = util.format('%s', data);
        logToFileAndConsole(log);
    });

    p.stderr.on('data', function(data) {
        var log = util.format('%s', data);
        if(log.indexOf('Couldn\'t connect to selenium server') !== -1){
            console.log('Selenium server not available')
        } else if(log.indexOf('Session ID is null') !== -1){
            return;
        } else {
            console.log(log)
        }
    });

    p.on('exit', function() {
        logToFileAndConsole('--------Runner finished at '+ new Date().toTimeString()+'-----------\n');
    });
}

function logToFileAndConsole(msg){
    console.log(msg);
    if(process.env.IS_TEST !== 'true') fs.appendFile(logFilePath, msg);
}

function execute(){
    logToFileAndConsole('--------Runner started at '+ new Date().toTimeString()+'------------\n');

    var filteredData = _.remove(dataSet, function(data){
        return !_.some(data, _.isEmpty)
    });

    filteredData.forEach(function(data){
        spawnProcess(data);
    });
}

execute();
