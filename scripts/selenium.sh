#/!bin/sh

echo "SELENIUM STARTED AT: $(date)"
export PATH=/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin
cd ~/Documents/sportshub-automation/

npm run selenium
