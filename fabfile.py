#!/usr/bin/env python
import json
import gspreadData
from fabric.api import *

def schedule_to_file():
	schedules = gspreadData.get_data_from_sheet()
	with open('data.json', 'w') as f:
		f.write(json.dumps(schedules))
